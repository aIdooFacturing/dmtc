<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>

<script type="text/javascript">

	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/svg_controller.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/svgController.js"></script>
<%-- <script type="text/javascript" src="${ctxPath }/js/chart/SVG_.js"></script> --%>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/svg.js/2.6.4/svg.min.js"></script>

<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<%-- <script type="text/javascript" src="${ctxPath }/js/chart/SVG_draggable.js"></script> --%>
<script type="text/javascript" src="${ctxPath }/js/chart/svg.draggable.min.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden;
	background-color: black;
  	font-family:'Helvetica';
}

</style> 
<script type="text/javascript">
var isLamp = false;

function getPmcData(){	
	$("#pmc-btn").css({
		"color" : "white",
		"background" : "black"		
	});	
	
	$("#lamp-btn").css({
		"color" : "#999999",
		"background" : "#666666"
	});
	
	isLamp = false;	
}

function getLampData(){
	$("#pmc-btn").css({
		"color" : "#999999",
		"background" : "#666666"			
	});	
	
	$("#lamp-btn").css({
		"color" : "white",
		"background" : "black"			
	});
	
	isLamp = true;
}


$(window).unload(function() { alert('Handler for .unload() called.'); });
	var loopFlag = null;
	var session = window.localStorage.getItem("auto_flag");
	if(session==null) window.localStorage.setItem("auto_flag", false);
	
	var flag = false;
	function stopLoop(){
		var flag = window.localStorage.getItem("auto_flag");
		
		if(flag=="true"){
			flag = "false"
				
		}else{
			flag = "true"
		}
		
		window.localStorage.setItem("auto_flag", flag);
		
		if(flag=="false"){
			clearInterval(loopFlag);	
		}else{
			startPageLoop();	
		};
		
		alert("페이지 자동 이동 : " + flag);
	};
			
	//var canvas;
	//var ctx;
	//var SETTIMEOUT = null;
				
	var handle = 0;
	$(function(){
		
		// wilson_버튼 
		$("#pmc-btn, #lamp-btn").css({
			"font-size" : getElSize(50),
			"border" : getElSize(1) + "px solid white",
			"font-weight" : "bolder",
			"padding-right" : getElSize(30),
			"padding-left" : getElSize(30)
		});
		
		$("#pmc-btn").css({
			"color" : "white",
			"background" : "black"
		});	
		
		$("#lamp-btn").css({
			"color" : "#999999",
			"background" : "#666666"
		});
		
		$("#pmc_lamp").css({
			"position" : "absolute",
			"top" : getElSize(175),
			"right" : getElSize(100)
		});		
				
		$("#bg_img").css({
			"width" : getElSize(3200),
			"position": "relative",
    		"top": getElSize(-250)
		});
		//drawCircle();
		//createNav("monitor_nav",0);

	
		/* canvas = document.getElementById("canvas");
		ctx = canvas.getContext("2d");
		canvas.width = contentWidth;
		canvas.height = contentHeight; */
		
		/* $("#canvas").css({
			"z-index" : -7,
			"position" : "absolute",
			"top" : marginHeight,
			"left" : marginWidth
		}); */
		
		//drawGroupDiv();
		//document.oncontextmenu = function() {stopLoop()}; 
		//setDivPos();
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		// 30분 단위로 새로고침 
		window.setInterval(function(){
			location.reload();			
		},1000*60*30);
		
		/* if(session=="false"){
			clearInterval(loopFlag);	
		}else{
			startPageLoop();	
		}; */
		
		chkBanner();
	});
	
	function drawGroupDiv(){
		
		
	};
	
	function startPageLoop(){
		loopFlag = setInterval(function(){
			location.href=ctxPath + "/chart/singleChartStatus.do";
			
		},1000*5);
	};
	
	function setDivPos(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			//"background-color" : "black",
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999,
			"pointer-events": "none"
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width(),
		})
		
		$("img").css({
			"display" : "inline"	
		});
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$("#status_chart").css({
			"width" : getElSize(450),
			"position" : "absolute",
			"z-index" : 9,
		});
		
		$("#status_chart").css({
			//"left" : $("#m_status").offset().left + ($("#m_status").width()/2) - ($("#status_chart").width()/2),
			"left" : marginWidth + getElSize(600),
			"bottom" : marginHeight + getElSize(20)
		});
		
		$("#svg").css({
			"width" : $("#table").width() - $("#svg_td").offset().left + marginWidth ,
			"height" : $("#svg_td").height(),
			"left" : $("#svg_td").offset().left,
			"top" : $("#svg_td").offset().top,
			"position" :"absolute",
			"z-index" : 9
		});
		
		$("#intro").css({
			"font-size" : getElSize(100),
			"pointer-events": "none"
		});
		
		$("#legend_table").css({
			"position" : "absolute",
			"z-index" : 999,
			"bottom" : getElSize(20),
			"left" : getElSize(1100) 
		});
		
		$("#legend_table td").css({
			"color" : "white",
			"font-size" : getElSize(20)
		});
		
		$("#incycle_legend, #wait_legend, #alarm_legend, #noconn_legend, #condition_legend, #offset_legend").css({
			"border-radius" : "50%",
			"width" : getElSize(40),
			"height" : getElSize(40)
		});
		
		$("#offset_chg_legend, #cdt_chg_legend").css({
			"width" : getElSize(50) + "px"
		});
		
		/* $("#condition_legend").css({
			"border-radius" : "50%",
			"border" : getElSize(10) + "px solid #7100D8",
			"margin-left" : getElSize(40)
		});
		
		$("#offset_legend").css({
			"border-radius" : "50%",
			"border" : getElSize(10) + "px solid #0278FF",
			"margin-left" : getElSize(40)
		}); */
		
		
		$("#label").css({
			"position" : "absolute",
			"top" : getElSize(195),
			"right" :getElSize(230),
			"color" : "white",
			"font-size" : getElSize(41)
		})
	/*
		* Date : 19.05.14 
		* Author : wilson
		* change "top", "left", "width"
	*/
	
		if(getParameterByName('lang')=='ko'){
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(100),
				"position" : "absolute",
				"top" : marginHeight + getElSize(85),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else if(getParameterByName('lang')=='en' || getParameterByName('lang')=='de'){
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(70),
				"position" : "absolute",
				"top" : marginHeight + getElSize(115),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else{
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(90),
				"position" : "absolute",
				"top" : marginHeight + getElSize(100),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}
		
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	function getParameterByName(name) {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
	
	
	/* function signIn(){
		dialog.open();
		
		$("#id-input, #pw-input, #pw-input-repeat").val("")
		$("#id-alert").html("")
		$("#pw-alert").html("")
	}
	
	function checkId(e){
		if($("#id-input").val().length>=5){
			var idRegex = /^[a-z0-9]+$/;
			var validId = $("#id-input").val().match(idRegex);
			
		    if(validId == null){
		    	$("#id-alert").css({
					"color" : "red"
				})
				passId=false;
		    	$("#id-alert").html("아이디에 공백 또는 특수 문자는 포함 될수 없습니다.")
		        return false;
		    }else{
		    	
		    	var url = ctxPath + "/chart/checkId.do";
		    	var param = "shopId=" + shopId +
		    				"&id=" + $("#id-input").val();
		    	
		    	$.ajax({
		    		url : url,
		    		data : param,
		    		type : "post",
		    		dataType : "text",
		    		success : function(data){
		    			var checkedId = data
		    			console.log(checkedId)
		    			if(checkedId=="true"){
		    				passId=false;
		    				$("#id-alert").css({
								"color" : "red"
							})
		    				$("#id-alert").html("이미 존재하는 아이디 입니다.");
		    			}
		    			if(checkedId=="false"){
		    				passId=true;
		    				$("#id-alert").html("사용하실수 있는 아이디 입니다.");
		    				$("#id-alert").css({
		    					"color" : "green"
		    				})
		    			}
		    		}
		    	});
		    	
		    }
		}else{
			$("#id-alert").html("아이디는 5자 이상입니다.")
		}
	}
	
	function checkPw(e){
		if($("#pw-input").val().length>=8 && $("#pw-input-repeat").val().length>=8){
			if($("#pw-input").val()==$("#pw-input-repeat").val()){
				passPw=true;
				$("#pw-alert").css({
					"color" : "green"
				})
				$("#pw-alert").html("패스워드가 같습니다.");
			}else{
				passPw=false;
				$("#pw-alert").css({
					"color" : "red"
				})
				$("#pw-alert").html("서로 다른 패스워드입니다.");
			}
		}else{
			passPw=false;
			$("#pw-alert").css({
				"color" : "red"
			})
			$("#pw-alert").html("패스워드는 8자 이상입니다.");
		}
	}
	
	var passId=false;
	var passPw=false;
	
	function doSignIn(){
		if(passId && passPw){
			console.log("가입가능");
			
			var url = ctxPath + "/chart/doSignIn.do";
	    	var param = "shopId=" + shopId +
	    				"&id=" + $("#id-input").val() +
	    				"&password=" + $("#pw-input").val();
			
			$.ajax({
	    		url : url,
	    		data : param,
	    		type : "post",
	    		dataType : "text",
	    		success : function(data){
	    			if(data){
	    				alert("회원 가입이 성공적으로 되었습니다.");
	    				dialog.close();
	    			}else{
	    				alert("회원 가입 실패");
	    			}
	    		}
	    	});
			
		}else{
			console.log("가입불가");
		}
	}
	 */
	 

	
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="svg"></div>
	<div id="time"></div>
	<div id="title_right"></div>
	<img alt="" src="${ctxPath }/images/status_chart.png?dummy=<%=new java.util.Date() %>" id="status_chart">
	
	<div id="dashBoard_title">${layout_title}</div>
	
	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" style="display: none">
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right' style="display: none">
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/monitor_left.png" class='menu_left'  style="display: none">
				</td>
				<td >
					<img id="title_image" alt="" src="${ctxPath }/images/blue_right.png" class='menu_right' style="display: none">
					<span id="pmc_lamp">
						<button id="pmc-btn" onclick="getPmcData()">PMC</button>					
						<button id="lamp-btn" onclick="getLampData()">LAMP</button>						
					</span>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'  ></span>
					<img alt="" src="${ctxPath }/images/selected_blue.png" class='menu_left' style="display: none">
				</Td>
				<td rowspan="10" id="svg_td">
					<table id="legend_table">
						<Tr>
							<Td>
								<div id="incycle_legend" style="background-color: rgb(28,198,28)"></div>
							</Td>
							<td>
								Cutting
							</td>
							<Td rowspan="2">
								<!-- <div id="condition_legend"></div> -->
								<%-- <img src="${ctxPath }/images/condition_chg.png" id="cdt_chg_legend"> --%>
							</Td>
							<td rowspan="2">
							<!-- 	Changed<br>Condition -->
							</td>
						</Tr>
						<tr>
							<Td>
								<div id="wait_legend" style="background-color: yellow"></div>
							</Td>
							<td>
								Waiting
							</td>
						</tr>
						<Tr>
							<Td>
								<div id="alarm_legend" style="background-color: #FF0000"></div>
							</Td>
							<td>
								Alarm
							</td>
							<Td rowspan="2">
								<!-- <div id="offset_legend" ></div> -->
								<%-- <img src="${ctxPath }/images/offset_chg.png" id="offset_chg_legend"> --%>
							</Td>
							<td rowspan="2">
							<!-- 	Changed<br>Offset -->
							</td>
						</Tr>
						<tr>
							<Td>
								<!-- <div id="noconn_legend" style="background-color: #323435"></div> -->
								<div id="noconn_legend" style="background-color: rgb(208,210,211)"></div>
							</Td>
							<td>
								Power off
							</td>
						</tr>
					</table>
					<center>
						<img alt="" src="${ctxPath }/images/DashBoard/Road_dmtc.png" id="bg_img"  style="width:93%">
						<!-- <img alt="" src="/Device_Status/resources/upload/road.png" id="bg_img"  style="width:93%"> -->
					</center> 
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span  class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span  class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none">
				</Td>
			</Tr>
		</table>
	</div>
	
  	<canvas id="canvas"></canvas>
	
	<div id="intro_back" style="display: none;"></div>
	<span id="intro"></span>
		
	<%-- <div id="title_main" class="title"><spring:message code="layout"></spring:message></div> --%>
</body>
</html>	
