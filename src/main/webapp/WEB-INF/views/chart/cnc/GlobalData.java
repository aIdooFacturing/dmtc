package com.unomic.cnc;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.StringTokenizer;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.unomic.cnc.communication.params.ReqStatusParam;
import com.unomic.cnc.data.MemoryMap;


@Component
public class GlobalData {
	private static final Logger logger = LoggerFactory.getLogger(GlobalData.class);
	//private static final String serverIP = "192.168.0.29:8080";
	//private static final String serverIP = "106.240.234.116:8080";
	private static final String serverIP = "106.240.234.114:8080";
	
	//private String TCP_IP = "106.240.234.116";
	//private final String TCP_IP = "192.168.0.5";
	//private final String TCP_PORT= "9999";

	private ConnectionController conn;
	private MemoryMap map;
	private String today = "empty";
	private String pMinute = "";
	private boolean first = true;
	private ArrayList<Double> arrStatusTime = new ArrayList<Double>();
	private ArrayList<String> arrStatusColor = new ArrayList<String>();
	private int idx = 0;
	private String pStatus = "";
	private String spindle = "";
	private String feed = "";
	private String progName = "";
	private String alarmNo = "";
	private String status = "";
	private String alarmContents = "";
	private int accCnt = 1;
	
	//server = unomic || client = doosan
	private final static String target = "server";
		
	private String from = "jay.kim@unomic.com";
	private String subject = "Network Error";
	
	//@PostConstruct
	public void init() {
		logger.info("Run init");
		//this.crtDvcs = new Devices();
		//this.testDvc = 3;
		
		if(target.equals("client")){
			//this.conn = new ConnectionController(this.TCP_IP, this.TCP_PORT);
			//this.conn.connect();
			this.map = new MemoryMap();
		};
	};
	
	//@Scheduled(fixedDelay = 1000)
	/*
	public void update(){
		logger.info("Run update");
		if(target.equals("client")){
			this.map = conn.getDataMap(new ReqStatusParam(0, 0));
			if(this.map== null){
				logger.info("reconnect");
				this.conn.disconnect();
				this.conn = new ConnectionController(this.TCP_IP, this.TCP_PORT);
				this.conn.connect();
				//sendMail();
			}
		};
		
		Date d = new Date();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String today = sdf.format(d);

		if(this.today.equals("empty")){
			this.today = today;
			File statusFile = new File(today + ".txt");
			if(statusFile.exists()){
				chkInvalidate(today);
			};
		}else if(!this.today.equals(today)){
			setStatusData();
      	};

      	this.today = today;
      	if(target.equals("client")){
      		writeStatus();
      	};
	};
	*/
	public void chkInvalidate(String txt){
		String result = "";
		File aFile = new File(txt + ".txt");
		FileReader fileReader;
		try {
			fileReader = new FileReader(aFile);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			logger.error(e.toString());
			return;
		}
		BufferedReader reader = new BufferedReader(fileReader);
		String line = null;
		
		try {
			while((line = reader.readLine())!=null){
				result += line;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e.toString());
		}
		try {
			reader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e.toString());
		}
		
		String[] index = result.split("/");
		String timeArray = index[0];
		String colorArray = index[1];
		
		String[] timeIdx = timeArray.split(",");
		String[] colorIdx = colorArray.split(",");
		
		this.idx = timeIdx.length;
		
		//txt 파일에 저장 된 시간 합
		double recodedTime = 0.00;
		
		for(int i = 0; i < timeIdx.length; i++){
			recodedTime += Double.parseDouble(timeIdx[i]);
		};
		
		//현재 시간과 비교
		Date d = new Date();
	     
		SimpleDateFormat sdf = new SimpleDateFormat("HH");
		SimpleDateFormat sdf2 = new SimpleDateFormat("mm");
		int hour = Integer.parseInt(sdf.format(d));
		double minute = Double.parseDouble(sdf2.format(d).substring(0,1));
		double currentTime = hour + 0.16*minute; 

		//기록되지 않은 값
		DecimalFormat df=new DecimalFormat("###,##0.00");
		String lossTime = df.format(currentTime - recodedTime);		

		logger.info("currentTime:"+currentTime);
		logger.info("recodedTime:"+recodedTime);
		logger.info("lossTime:"+lossTime);
		
		//현재 시간보다 기록시간이 작으면
		if(recodedTime<currentTime){
			//배열 초기화 후 값 삽입
			arrStatusTime = new ArrayList<Double>();
			arrStatusColor = new ArrayList<String>();
			
			for(int i = 0; i < timeIdx.length; i++){
				arrStatusTime.add(Double.parseDouble(timeIdx[i]));
			};
			arrStatusTime.add(Double.parseDouble(lossTime));
			
			for(int i = 0; i < colorIdx.length; i++){
				arrStatusColor.add(colorIdx[i]);
			};
			arrStatusColor.add("white");
			
			this.first = false;
			
			Date date = new Date();
			SimpleDateFormat sdf3 = new SimpleDateFormat("HHmm");
			String minute2 = sdf3.format(date);
			minute2 = minute2.substring(2,3);
		}else{
			txtFileParsing();
		};
 	};
	
	public void txtFileParsing(){
		this.first = false;
		String txt = "";
		String statusFile = this.today;
		File aFile = new File(statusFile+".txt");
		FileReader fileReader;
		try {
			fileReader = new FileReader(aFile);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			logger.error(e.toString());
			return;
		}
		BufferedReader reader = new BufferedReader(fileReader);
		
		String line = null;
		
		try {
			while((line = reader.readLine())!=null){
				txt += line;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			logger.error(e.toString());
		};
		
		try {
			reader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error(e.toString());
		}
		
		//txt 파일 파싱
		String[] index = txt.split("/");
		String timeArray = index[0];
		String colorArray = index[1];
		
		String[] time = timeArray.split(",");
		String[] color = colorArray.split(",");
		
		this.idx = time.length-1;
		
		//배열 초기화
		this.arrStatusTime = new ArrayList<Double>();
		this.arrStatusColor = new ArrayList<String>();
		
		for(int i = 0; i < time.length;i++){
			arrStatusTime.add(Double.parseDouble(time[i]));
		};
		
		for(int i = 0; i < color.length;i++){
			arrStatusColor.add(color[i]);
		};
		
		Date d = new Date();
		SimpleDateFormat sdf2 = new SimpleDateFormat("HHmm");
		String minute = sdf2.format(d);
		minute = minute.substring(2,3);
		this.pMinute = minute;
	};
	
	public void writeStatus() {
		String mcStatus = null;
		int feedOverride = 0;
		
		if(map!=null){
			feedOverride = map.getFeed_override();
			mcStatus = map.getNcStatus();

			String targetFeed = map.getTargetFeed();
			Integer targetIndex = targetFeed.indexOf(".");
			targetFeed = targetFeed.substring(0, targetIndex);
		}else{
			logger.info("@ write Stsatus : cannon fail");
			return;
		};
		
		Date d = new Date();

		//현재 시간(10분 단위)
      SimpleDateFormat sdf2 = new SimpleDateFormat("HHmm");
      String minute = sdf2.format(d);
      String hour = minute.substring(0,2);
      minute = minute.substring(2,3);
      
      //status Color set
      String statusColor = "";
      String status = map.getNcStatus();
      	String alarm;
		try {
			alarm = URLEncoder.encode(map.getAlarmArray(),"utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			logger.error(e.toString());
			return;
		}
      if(status.equals("STRT") || status.equals("START")){
    	  statusColor = "green";
      }else if((!status.equals("STRT") || !status.equals("START")) && alarm.equals("")){
    	  statusColor = "yellow";
      }else if(!alarm.equals("")){
    	  statusColor = "red";
      };
      
      	//차트 한 칸 크기 
      	double time = (Double.parseDouble(hour) + 0.16 * Double.parseDouble(minute));

      	//첫 번째 칸 채움 (0 ~ 시작 점)
      	if(this.first){
      		if(time>0){
      			time -= 0.16;
      		};
      		this.arrStatusTime.add(time);
      		this.arrStatusColor.add("white");
			this.first = false;
      	};
      	
      	//10분 단위 변경 시 데이터 추가
      	if(!pMinute.equals(minute)){
      		this.arrStatusTime.add(0.16);
      		this.arrStatusColor.add(statusColor);	
      		this.pMinute = minute;
      		this.idx++;
      	};
      	
      	//알람 or 초록불에서 노랑불이면 arrayData 변경
      	if(statusColor.equals("red") || 
      			(pStatus.equals("green") && statusColor.equals("yellow") && !this.arrStatusColor.get(idx).equals("red"))){
  			this.arrStatusColor.set(this.idx,statusColor);
  			this.pMinute=minute;
      	};
		
		this.pStatus = statusColor;
		
		BufferedWriter statusFile;
		try {
			statusFile = new BufferedWriter(new FileWriter(this.today + ".txt"));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			logger.error(e1.toString());
			return;
		}
		
		String statusChartData = "";
		String contents = "";
		String statusTimeTxt = "";
		String statusColorTxt = "";
		String comma = "";
		String comma2 = "";
		for(int i = 0; i < this.arrStatusTime.size(); i++){
			statusTimeTxt += comma + arrStatusTime.get(i);
			comma = ",";
		};
		
		for(int i = 0; i < this.arrStatusColor.size(); i++){
			statusColorTxt += comma2 + arrStatusColor.get(i);
			comma2 = ",";
		};
		
		statusChartData = statusTimeTxt + "/" + statusColorTxt;
		
		try {
			contents += statusChartData+ "/" +
					map.getSpindlesValues() + "/" + 
					feedOverride + "/" + 
					map.getMainProg_name() + "/" + 
					map.getAlarmNumber() + "/" + 
					mcStatus + "/" + 
					URLEncoder.encode(map.getAlarmArray(),"utf-8") + "/" + 
					map.getAlarm() + "/" +
					this.accCnt;
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			logger.error(e.toString());
		}
		accCnt++;
		
		try {
			statusFile.write(contents);
			statusFile.flush();
			statusFile.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			logger.error(e.toString());
		}
		sendData(contents);
	};
	
	public String getToday(){
		return this.today;
	};
	
	public MemoryMap getStatusMap(){
		return this.map;
	};
	
	public ArrayList<Double> getStatusTime(){
		return this.arrStatusTime;
	};
	
	public ArrayList<String> getStatusColor(){
		return this.arrStatusColor;
	};
	
	public String getSpindle() {
		return spindle;
	};

	public void setSpindle(String spindle) {
		this.spindle = spindle;
	};
	
	public String getFeed() {
		return feed;
	};

	public void setFeed(String feed) {
		this.feed = feed;
	};
	
	public String getProgName() {
		return progName;
	};

	public void setProgName(String progName) {
		this.progName = progName;
	};
	
	public String getAlarmNo() {
		return alarmNo;
	};

	public void setAlarmNo(String alarmNo) {
		this.alarmNo = alarmNo;
	};
	
	public String getStatus() {
		return status;
	};

	public void setStatus(String status) {
		this.status = status;
	};
	
	public String getAlarmContents() {
		return alarmContents;
	};

	public void setAlarmContents(String alarmContents) {
		this.alarmContents = alarmContents;
	};
	
	public void setAccCnt(int accCnt){
		this.accCnt = accCnt;
	};
	
	public int getAccCnt(){
		return accCnt;
	};
	
	public void setStatusData(){
		this.arrStatusTime = new ArrayList<Double>();
		this.arrStatusColor = new ArrayList<String>();
		this.idx = 0;
		this.first = true;
		this.pMinute = "";
		this.pStatus = "";
	};
	
	public void resetStatusArray(){
		this.arrStatusTime = new ArrayList<Double>();
		this.arrStatusColor = new ArrayList<String>();
	};
	
	public void sendData(String data) {
		try{
			URL url = new URL("http://" + serverIP + "/DULink/nfc/receiveData.do");
	        Map<String,Object> params = new LinkedHashMap();
	        String localhost = InetAddress.getLocalHost().toString();
	        StringTokenizer st = new StringTokenizer(localhost, "/");
	        String host = st.nextToken();
	        String ip = st.nextToken();
	        
	        params.put("data", data);
	        params.put("ip", ip);
	        
	        StringBuilder postData = new StringBuilder();
	        for (Map.Entry<String,Object> param : params.entrySet()) {
	            if (postData.length() != 0) postData.append('&');
	            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
	            postData.append('=');
	            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
	        }
	        byte[] postDataBytes = postData.toString().getBytes("UTF-8");

	        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
	        conn.setRequestMethod("POST");
	        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
	        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
	        conn.setDoOutput(true);
	        conn.getOutputStream().write(postDataBytes);

	        Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
	        String tmpLog="";
	        //for (int c; (c = in.read()) >= 0; System.out.print((char)c));
	        for (int c; (c = in.read()) >= 0; tmpLog += ((char)c) );
	        logger.info("ContentsOfBuffer:"+tmpLog);
		}catch(Exception e){
			//e.printStackTrace();
			logger.error(e.toString());
		};
	};
	
//	public void sendMail(){
//		try{
//			logger.info("sendMail.");
//			MimeMessage message = mailSender.createMimeMessage();
//			MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, "UTF-8");
//			messageHelper.setTo("jeongwan88@gmail.com");
//			messageHelper.setText("Network Error");
//			messageHelper.setFrom(from);
//			messageHelper.setSubject(subject);
//			mailSender.send(message);
//		}catch(Exception e){
//			logger.error("Failed send mail.");
//			logger.error(e.toString());;
//			return;
//		};
//		logger.info("success sendMail..");
//	};
};	
