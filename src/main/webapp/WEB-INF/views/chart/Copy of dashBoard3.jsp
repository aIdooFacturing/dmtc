<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<title>Dash Board</title>
<script type="text/javascript" src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="${ctxPath }/js/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<style type="text/css">
	.selected_span{
	background-color :white;
	color : #008900;
}
	
</style>

<script type="text/javascript">

	var targetWidth = 3840;
	var targetHeight = 2160;
	
	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	var shopId = 1;
	function getElSize(n){
		return originWidth/(targetWidth/n);
	};
	
	var loopFlag = null;
	var flag = false;
	function stopLoop(){
		flag = !flag;
		
		if(!flag){
			clearInterval(loopFlag);	
		}else{
			startPageLoop();	
		};
		
		alert("페이지 자동 이동 : " + flag);
	};
	
	var menu = false;
	function getMousePos(){
		$(document).on("mousemove", function(e){
			var target = $("#menu_btn").width();

			if((e.pageX <= target && e.pageY <= target) || panel){
				if(!menu){
					$("#menu_btn").animate({
						"left" : 0,
						"top" : getElSize(20)
					});
					menu = true;
				};
			}else{
				if(menu){
					$("#menu_btn").animate({
						"left" : -getElSize(100),
					});	
					menu = false;
				};
			};
		});
	};
	
	var panel = false;
	function showPanel(){
		$("#panel").animate({
			"left" : 0
		});
		
		$("#menu_btn").animate({
			"left" : contentWidth * 0.2 + getElSize(50)	
		});
		
		$("#corver").css({
			"z-index" : 99999
		});
	};

	function closePanel(){
		$("#panel").animate({
			"left" : -contentWidth * 0.2 - getElSize(40)
		});
		
		$("#menu_btn").animate({
			"left" : 0
		});
		
		$("#corver").css({
			"z-index" : -1
		});
	};
	
	var save_type = "init"
	
	$(function(){
		$("#next").click(nextPage)
		$("#prev").click(prevPage)
		
		setStartTime();
		//getMousePos();
		$("#menu_btn").click(function(){
			location.href = "${ctxPath}/chart/index.do"
		});
		
		
		setElement();
		
		
		$("#panel_table td").addClass("unSelected_menu");
		$("#menu3").removeClass("unSelected_menu");
		$("#menu3").addClass("selected_menu");
		
		
		setInterval(time, 1000);

		getDvcIdList();
	});
	
	var c_page = 1;
	var max_row = 20;

	
	function nextPage(){
		if($("svg").length<(max_row)) return;
		c_page++;
		if(c_page>1) $("#prev").addClass("enablePointer");
		getDvcIdList()
	};

	var dateArray = []
	function prevPage(){
		if(c_page<=1) return;
		c_page--;
		if(c_page==1) $("#prev").removeClass("enablePointer");
		getDvcIdList();
	}
	
	function getDvcIdList(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));

		var sDate = year + "-" + month + "-" + day;
		var eDate = year + "-" + month + "-" + day + " 23:59:59"; 
		
		var param = "sDate=" + sDate + 
					"&eDate=" + eDate + 
					"&shopId=" + shopId + 
					"&maxRow=" + max_row + 
					"&offset=" + ((c_page-1)*max_row);
		
		var url = "${ctxPath}/chart/getBarChartDvcId.do";
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dvcId;
				if(json.length>=max_row){
					$("#next").addClass("enablePointer");
				}else{
					$("#next").removeClass("enablePointer");
				}
				
				for(var i = 0; i < json.length; i++){
					drawBarChart2("status2_" + i, json[i].name);	
					getStatusChart2(json[i].dvcId, i);
				}
			}
		});
	};
	
	function getStatusChart2(dvcId, idx){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth() + 1));
		var day = addZero(String(date.getDate()));
		var hour = date.getHours();
		var minute = addZero(String(date.getMinutes())).substr(0,1);
		
		
		var today = year + "-" + month + "-" + day;
		
		var url = "${ctxPath}/chart/getTimeData.do";
		var param = "dvcId=" + dvcId + 
					"&workDate=" + today;
		
		setInterval(function (){
			var minute = String(new Date().getMinutes());
			if(minute.length!=1){
				minute = minute.substr(1,2);
			};
			
			if(minute==2 && eval("dvcMap" + idx).get("initFlag") || minute==2 && typeof(eval("dvcMap" + idx).get("initFlag")=="undefined")){
				getStatusChart2(dvcId, idx);
				console.log("init")
				eval("dvcMap" + idx).put("initFlag", false);
				eval("dvcMap" + idx).put("currentFlag", true);
			}else if(minute!=2){
				eval("dvcMap" + idx).put("initFlag", true);
			};
		}, 1000 * 10);
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				
				eval("dvcMap" + idx + " = new JqMap();");
				
				if(data==null || data==""){
					eval("dvcMap" + idx).put("noSeries", true);
					getCurrentDvcStatus(dvcId, idx);
					return;
				}else{
					eval("dvcMap" + idx).put("noSeries", false);
				};
	
				 
				 var status = $("#status2_" + idx).highcharts();
				var options = status.options;
					
				var json = data.statusList;
				
				var color = "";
				
				var status = json[0].status;
				if(status=="IN-CYCLE"){
					color = "green"
				}else if(status=="WAIT"){
					color = "yellow";
				}else if(status=="ALARM"){
					color = "red";
				}else if(status=="NO-CONNECTION"){
					color = "gray";
				};
				
				var blank;
				var f_Hour = json[0].startDateTime.substr(11,2);
				var f_Minute = json[0].startDateTime.substr(14,2);
				
				var startHour = 20;
				var startMinute = 3;
				
				var startN = 0;
				
				spdLoadPoint = [];
				spdOverridePoint = [];
				
				
				if(f_Hour==startHour && f_Minute==(startMinute*10)){
					options.series.push({
						data : [ {
							y : Number(20),
							segmentColor : color
						} ],
					});
				}else{
					if(f_Hour>=20){
						startN = (((f_Hour*60) + Number(f_Minute)) - ((startHour*60) + (startMinute*10)))/2; 
					}else{
						startN = ((24*60) - ((startHour*60) + (startMinute*10)))/2;
						startN += (f_Hour*60/2) + (f_Minute/2);
					};
					
					options.series.push({
						data : [ {
							y : Number(20),
							segmentColor : "gray"
						} ],
					});
						
					for(var i = 0; i < startN-1; i++){
						options.series[0].data.push({
							y : Number(20),
							segmentColor : "gray"
						});
						spdLoadPoint.push(Number(0));
						spdOverridePoint.push(Number(0));
					};
				};
				
				
				
				
				$(json).each(function(idx, data){
					if(data.status=="IN-CYCLE"){
						color = "green"
					}else if(data.status=="WAIT"){
						color = "yellow";
					}else if(data.status=="ALARM"){
						color = "red";
					}else if(data.status=="NO-CONNECTION"){
						color = "gray";
					};
					options.series[0].data.push({
						y : Number(20),
						segmentColor : color
					});
				});
				
				for(var i = 0; i < 719-(json.length+startN); i++){
					options.series[0].data.push({
						y : Number(20),
						segmentColor : "rgba(0,0,0,0)"
					});
				};
				
				
				
				 
				status = new Highcharts.Chart(options);
				//getCurrentDvcStatus(dvcId, idx);
			}
		});
	};
	
	function getCurrentDvcStatus(dvcId, idx){
		var url = "${ctxPath}/chart/getCurrentDvcData.do";
		var param = "dvcId=" + dvcId;
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var chartStatus = data.chartStatus;
				var name = data.name;
				
				var status = $("#status2_" + idx).highcharts();
				var options = status.options;

				if(eval("dvcMap" + idx).get("currentFlag") || typeof(eval("dvcMap" + idx).get("currentFlag"))=="undefined"){
					if(eval("dvcMap" + idx).get("noSeries")){
		      			options.series = [];
		      			options.series.push({
					        data: [{
					        		y : Number(200),
					        		segmentColor : slctChartColor(chartStatus) 
					        	}],
					    });
		      		}else{
		      			options.series[0].data.push({
			        		y : Number(200),
			        		segmentColor : slctChartColor(chartStatus)
			  			});		      			
		      		};

		      		var now = options.series[0].data.length;
					//var blank = 144 - now;
					var blank = 719 - now;
					
					for(var i = 0; i <= blank; i++){
						options.series[0].data.push({
			        		y : Number(200),
			        		segmentColor : "rgba(0,0,0,0.0)"
			  			});	
					};
					
					status = new Highcharts.Chart(options);
					eval("dvcMap" + idx).put("currentFlag", false);
				};
				setTimeout(function (){
					getCurrentDvcStatus(dvcId, idx);
				}, 3000);
				$("#dvcName" + idx).html(name);
			}
		});	
	};
	
	function setStartTime(){
		var url = "${ctxPath}/chart/getStartTime.do"
		
		$.ajax({
			url :url,
			dataType :"text",
			type : "post",
			success : function(data){
				startTime = Number(data);
				for(var i = 0; i<=24; i++){
					if(i+startTime>24){
						startTime -= 24;
					};
					
					timeLabel.push(i+startTime);
					for(var j = 0; j < 5; j++){
						timeLabel.push(0);	
					};
				};					
			}
		});
	};
	
	var timeLabel = [];
	
	function slctChartColor(status){
		var color;
		if(status.toLowerCase()=="in-cycle"){
			color = colors[0];
		}else if(status.toLowerCase()=="wait"){
			color = colors[1];
		}else if(status.toLowerCase()=="alarm"){
			color = colors[2];
		}else if(status.toLowerCase()=="no-connection"){
			color = colors[3];
		};
		
		return color;
	};
	
	var startTimeLabel = new Array();
	
	var startHour = 20;
	var startMinute = 3;
	
	var colors;
	
	function drawBarChart2(id, name){
		var fontColor = "white;"
		if(name=="NB13" || name=="NB14W"){
			fontColor = "black";
		}
		
		var m0 = "",
		m02 = "",
		m04 = "",
		m06 = "",
		m08 = "",
		
		m1 = "";
		m12 = "",
		m14 = "",
		m16 = "",
		m18 = "",
		
		m2 = "";
		m22 = "",
		m24 = "",
		m26 = "",
		m28 = "",
		
		m3 = "";
		m32 = "",
		m34 = "",
		m36 = "",
		m38 = "",
		
		m4 = "";
		m42 = "",
		m44 = "",
		m46 = "",
		m48 = "",
		
		m5 = "";
		m52 = "",
		m54 = "",
		m56 = "",
		m58 = "";
	
	var n = Number(startHour);
	if(startMinute!=0) n+=1;
	
	for(var i = 0, j = n ; i < 24; i++, j++){
		eval("m" + startMinute + "=" + j);
		
		startTimeLabel.push(m0);
		startTimeLabel.push(m02);
		startTimeLabel.push(m04);
		startTimeLabel.push(m06);
		startTimeLabel.push(m08);
		
		startTimeLabel.push(m1);
		startTimeLabel.push(m12);
		startTimeLabel.push(m14);
		startTimeLabel.push(m16);
		startTimeLabel.push(m18);
		
		startTimeLabel.push(m2);
		startTimeLabel.push(m22);
		startTimeLabel.push(m24);
		startTimeLabel.push(m26);
		startTimeLabel.push(m28);
		
		startTimeLabel.push(m3);
		startTimeLabel.push(m32);
		startTimeLabel.push(m34);
		startTimeLabel.push(m36);
		startTimeLabel.push(m38);
		
		startTimeLabel.push(m4);
		startTimeLabel.push(m42);
		startTimeLabel.push(m44);
		startTimeLabel.push(m46);
		startTimeLabel.push(m48);
		
		startTimeLabel.push(m5);
		startTimeLabel.push(m52);
		startTimeLabel.push(m54);
		startTimeLabel.push(m56);
		startTimeLabel.push(m58);
		
		if(j==24){ j = 0}
	};
	
		
		var perShapeGradient = {
	            x1: 0,
	            y1: 0,
	            x2: 1,
	            y2: 0
	        };
	        colors = Highcharts.getOptions().colors;
	        colors = [{
	            linearGradient: perShapeGradient,
	            stops: [
	                [0, '#148F01'],
	                [1, '#1ABB02']
	                ]
	            }, {
	            linearGradient: perShapeGradient,
	            stops: [
	                [0, '#C7C402'],
	                [1, '#F5F104']
	                ]
	            }, {
	            linearGradient: perShapeGradient,
	            stops: [
	                [0, '#ff0000'],
	                [1, '#FF2626']
	                ]
	           },{
		            linearGradient: perShapeGradient,
		            stops: [
		                [0, '#8c9089'],
		                [1, '#A9ADA6']
		                ]}
	        ]
	         
		var options = {
			chart : {
				type : 'coloredarea',
				backgroundColor : 'rgba(255, 255, 255, 0)',
				height : $("#mainTable").height()*0.9/11, 
				marginTop: -60,
				marginBottom: 25
			},
			credits : false,
			exporting: false,
			title : {
				text :name,
				align :"left",
				y:10,
				style : {
					color : "black",
					fontSize: getElSize(40) + "px",
					fontWeight: 'bold'
				}
			},
			yAxis : {
				labels : {
					enabled : false,
				},
				title : {
					text : false,
	                rotation: 0,
	                "textAlign": 'right',
	                x:100,
	                y: -getElSize(10),
					style : {
						color : "black",
						fontSize: getElSize(20) + "px",
						fontWeight: 'bold'
					}
				},
			},
			xAxis:{
		           categories:startTimeLabel,
		            labels:{
		            	step: 1,
						formatter : function() {
							var val = this.value

							return val;
						},
			                        style :{
			    	                	color : fontColor,
			    	                	fontSize : "9px"
			    	                },
		            }
		        },
		       
			tooltip : {
				headerFormat : "",
				style : {
					fontSize : '10px',
				},
				enabled : false
			}, 
			plotOptions: {
			    line: {
			        marker: {
			            enabled: false
			        }
			    },
			    series : {
			    	animation : false
			    }
			},
			legend : {
				enabled : false
			},
			series: []
		}

	   	$('#' + id).highcharts(options);
	};
	
	function time(){
		var date = new Date();
		var month = date.getMonth()+1;
		var date_ = addZero(String(date.getDate()));
		var day = date.getDay();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		if(day==1){
			day = "Mon";
		}else if(day==2){
			day = "Tue";
		}else if(day==3){
			day = "Wed";
		}else if(day==4){
			day = "Thu";
		}else if(day==5){
			day = "Fri";
		}else if(day==6){
			day = "Sat";
		}else if(day==0){
			day = "Sun";
		};
		
		$("#date").html(month + " / " + date_ + " (" + day + ")");
		$("#time").html(hour + " : " + minute + " : " + second);
	};
	
	function setElement(){
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		
		$("hr").css({
			"left" : width/2 - $("hr").width()/2
		});
		
		$("#mainTable").css({
			"left" : width/2 - $("table").width()/2
		});
		
		$(".status").css({
			"width" : contentWidth*0.45,
			"height" : contentHeight*0.9/11,
			"margin-bottom": -contentHeight/(targetHeight/10)	
		});
		
		$("#container").css({
			"width": contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-top" : height/2 - ($("#container").height()/2),
			"margin-left" : width/2 - ($("#container").width()/2)
		});
		
		$("#title_main").css({
			"font-size" : getElSize(100),
			"top" : $("#container").offset().top + (getElSize(50)),
			"color" : "white",
			"font-weight" : "bolder"
		});
		
		$("#title_main").css({
			"left" : originWidth/2 - ($("#title_main").width()/2)
		});
		
		$("#title_left").css({
			"width" : getElSize(300),
			"left" : $("#container").offset().left + getElSize(50),
			"top" : $("#container").offset().top + (getElSize(50))
		});
		
		$("#title_right").css({
			"right" : $("#container").offset().left,
			"color" : "white",
			"font-weight" : "bolder",
			"font-size" : getElSize(40),
			"top" : $("#container").offset().top + (getElSize(50))
		});
		
		$("#time").css({
			"top": $("#container").offset().top + getElSize(170),
			"right": $("#container").offset().left + getElSize(50),
			"font-size" : getElSize(30)
		});
		
		$("#date").css({
			"top": $("#container").offset().top + getElSize(170),
			"right": $("#container").offset().left + getElSize(250),
			"font-size" : getElSize(30)
		});
		
		$("hr").css({
			"width" : contentWidth * 0.95,
			"top" : $("#container").offset().top + getElSize(200),
		});
		
		$("hr").css({
			"left" : $("#container").offset().left + contentWidth/2 - $("hr").width()/2,
		});
		
		$("#mainTable").css({
			"width" : contentWidth * 0.95,
			"top" : $("#container").offset().top + getElSize(250)
		});
		
		$("#mainTable").css({
			"left" : $("#container").offset().left + contentWidth/2 - $("#mainTable").width()/2,
		});
		
		$("#menu_btn").css({
			"position" : "absolute",
			"width" : getElSize(100),
			"left" : 0,
			"top" : getElSize(20),
			"cursor" : "pointer",
			"z-index" : 999999
		});
		$("#panel").css({
			"background-color" : "rgb(34,34,34)",
			"border" : getElSize(20) + "px solid rgb(50,50,50)",
			"position" : "absolute",
			"width" : contentWidth * 0.2,
			"top" : 0,
			"left" : -contentWidth * 0.2 - getElSize(40),
			"z-index" : 99999
		});
		
		$("#panel_table").css({
			"color" : "white",
			"font-size" : getElSize(40)
		});

		$("#panel_table td").css({
			"padding" : getElSize(25),
			"cursor" : "pointer"
		});
		
		$("#panel_table td").addClass("unSelected_menu");
		
		
		$(".machineListForTarget").css({
			"position" : "absolute",
			"width" : getElSize(1200),
			"height" : getElSize(1200),
			"overflow" : "auto",
			//"top" : getElSize(50),
			//"background-color" : "rgb(34,34,34)",
			"background-color" : "green",
			"color" : "white",
			"font-size" : getElSize(50),
			"padding" : getElSize(50),
			"overflow" : "auto",
			"border-radius" : getElSize(50),
			"border" : getElSize(10) + "px solid white",
		});
		
		$(".machineListForTarget").css({
			"left" : (originWidth/2) - ($(".machineListForTarget").width()/2),
		});
		
		$("#prev, #next").css({
			"width" : getElSize(100),
			"margin" : getElSize(50),
			"margin-bottom" : getElSize(200)
		});
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
</script>
<style>
.enablePointer{
		cursor : pointer;
}
#container{
	background: url("../images/DashBoard/Background.png");
	background-size : 100% 100%;
	background-color: white;
}

body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	background-color: black;
  	font-family:'Helvetica';
	background-size : 100% 100%;
	overflow: hidden;
}


#title_main{
	z-index: 999;
	position:absolute;
}
#title_left{
	position:absolute;
	left : 50px;
	width: 300px;
}

#title_right{
	position:absolute;	
	right: 50px;
}

#time{
	font-size : 30px;
	color: white;
	position:absolute;
} 
#date{
	font-size : 30px;
	color: white;
	position:absolute;
} 

hr{
	z-index: 99;
	border: 2px solid white;
	position: absolute;
}

#mainTable{
	z-index: 99;
	position: absolute;
}
.leftTd{
	border-right: 5px solid white; width: 50%; padding: 0 0 0 0";
}
.status{
	margin-top: 0px;
	margin-bottom: -10px;
}

.unSelected_menu{
	background-color :gray;
	color : white
}

.selected_menu{
	background-color :rgb(33,128,250);
	color : white
}

.menu:HOVER{
	background-color: white;
	color: gray;
}
</style>
<script type="text/javascript">
	
</script>

</head>


<body>
	<div id="machineListForTarget_day" style="opacity:0" class="machineListForTarget">
		<center>
			<table id="machineListTable" style="width: 100%">
			</table>
		</center>
	</div>
	<div id="machineListForTarget_night" style="opacity:0" class="machineListForTarget">
		<center>
			<table id="machineListTable" style="width: 100%">
			</table>
		</center>
	</div>	
	
	<div id="container" >
		<img src="${ctxPath }/images/home.png" id="menu_btn" >
		<div id="svg"></div>
	</div>
	<div id="popup" style="width: 95%;height:200px;background-color:white; position: absolute; display: none;">
		<div id='popupChart' style="width: 100%" ></div>
	</div>
	
	<div id="title_main" class="title"><spring:message code="24barchart"></spring:message> </div>	
	<div id="title_right" class="title"></div>	
	
	<font id="time"></font>
	<font id="date"></font>
	<hr>
	<Center>
		<table id="mainTable" style="border-collapse: collapse;">
			<tr>
				<td class="leftTd">
					<div id="status2_0" class="status"></div>
				</td>
				<td>
					<div id="status2_1" class="status"></div>			
				</td>
			</tr>
			<tr>
				<td class="leftTd">
					<div id="status2_2" class="status"></div>
				</td>
				<td>
					<div id="status2_3" class="status"></div>
				</td>
			</tr>
			<tr>
				<td class="leftTd">
					<div id="status2_4" class="status"></div>
				</td>
				<td>
					<div id="status2_5" class="status"></div>
				</td>
			</tr>
			<tr>
				<td class="leftTd">
					<div id="status2_6" class="status"></div>
				</td>
				<td>
					<div id="status2_7" class="status"></div>
				</td>
			</tr>	
			<tr>		
				<td class="leftTd">
					<div id="status2_8" class="status"></div>
				</td>
				<td>
					<div id="status2_9" class="status"></div>
				</td>
			<tr>	
				<td class="leftTd">
					<div id="status2_10" class="status"></div>
				</td>
				<td>
					<div id="status2_11" class="status"></div>
				</td>
			</tr>	
			<tr>
				<td class="leftTd">
					<div id="status2_12" class="status"></div>
				</td>
				<Td>
					<div id="status2_13" class="status"></div>
				</Td>
			</tr>
			<tr>
				<td class="leftTd">
					<div id="status2_14" class="status"></div>
				</td>
				<td>
					<div id="status2_15" class="status"></div>
				</td>	
			</tr>
			<tr>
				<td class="leftTd">
					<div id="status2_16" class="status"></div>
				</td>
				<td>
					<div id="status2_17" class="status"></div>
				</td>
			</tr>	
			<tr>		
				<Td class="leftTd">
					<div id="status2_18" class="status"></div>
				</Td>	
				<td>
					<div id="status2_19" class="status"></div>
				</td>
			</tr>
			<Tr>
				<td colspan="2" align="center" style="padding: 0px">
					<img alt="" src="${ctxPath }/images/arrow_left_black.png" id="prev">
					<img alt="" src="${ctxPath }/images/arrow_right_black.png" id="next">
				</td>
			</Tr>	
		</table>
		
		
	</Center>
	
</body>
</html>