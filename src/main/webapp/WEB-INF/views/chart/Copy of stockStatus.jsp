<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<style type="text/css">
	.selected_span{
	background-color :white;
	color : #008900;
}
	
</style>
<title><spring:message code="stock_status" ></spring:message></title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return targetWidth / (contentWidth / n);
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script src="${ctxPath }/js/jquery-ui.js"></script>
<script src="${ctxPath}/js/highstock.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/default.css">
<style type="text/css">
	body{
		overflow :hidden;
	}
</style>
<script type="text/javascript">
var shopId = 1;

var save_type = "init"

function getPrdNo(){
	var url = "${ctxPath}/chart/getMatInfo.do";
	var param = "shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			json = data.dataList;
			
			var option = "<option value='ALL'>전체</option>";
			
			$(json).each(function(idx, data){
				option += "<option value='" + decode(data.prdNo) + "'>" + decode(data.prdNo) + "</option>"; 
			});
			
			$("#group").html(option);
			
			getStockStatus();
		}
	});
};

function showCorver(){
	$("#corver").css("z-index", 999);	
};

function hideCorver(){
	$("#corver").css("z-index", -999);
};

function bindEvt(){
	
	$("#insert #cancel, #update #cancel").click(closeInsertForm);
	$("#insert #lotCnt").keyup(calcAQL);
	$("#insert #notiCnt").keyup(calcAQL);
	$("#update #lotCnt").keyup(calcAQL_update);
	$("#update #notiCnt").keyup(calcAQL_update);
	
};

function closeInsertForm(){
	hideCorver();
	$("#insertForm, #updateForm").css("z-index",-9999);
	$("#updateForm #rcvCnt").css("color", " white");
	return false;
};

var preRCV_QTY = 0;
var preNOTI_QTY = 0;


var a;
function calcAQL(el){
	var lotCnt = $(el).parent("td").parent("tr").children("td:nth(5)").children("input").val();
	var notiCnt = $(el).parent("td").parent("tr").children("td:nth(7)").children("input").val();
	
	$(el).parent("td").parent("tr").children("td:nth(6)").html("5");
	if(notiCnt>0){
		$(el).parent("td").parent("tr").children("td:nth(8)").html("0").css("color","red");	
	}else{
		$(el).parent("td").parent("tr").children("td:nth(8)").html(lotCnt).css("color","white");
	}
};

function calcAQL_update(){
	var lotCnt = $("#update #lotCnt").val();
	var notiCnt = $("#update #notiCnt").val();
	
	$("#update #smplCnt").html("5");
	if(notiCnt>0){
		$("#update #rcvCnt").html("0").css("color","red");	
	}else{
		$("#update #rcvCnt").html(lotCnt).css("color","white");
	}
};

function getTime(){
	var date = new Date();
	var hour = addZero(String(date.getHours()));
	var minute = addZero(String(date.getMinutes()));
	var second = addZero(String(date.getSeconds()));
	
	return hour + ":" + minute;
};

function showUpdateForm(id){
	showCorver();
	matId = id;
	getStockInfo(id);
};


function getStockInfo(id){
	var url = "${ctxPath}/chart/getStockInfo.do";
	var param = "id=" + id;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			$("#update #id").html(data.id);
			$("#update #comName").html(data.comName);
			$("#update #prdNo").html(data.prdNo);
			$("#update #spec").html(data.spec);
			$("#update #lotNo").val(data.lotNo);
			$("#update #smplCnt").html(data.smplCnt);
			$("#update #rcvCnt").html(data.rcvCnt);
			$("#update #lotCnt").val(data.lotCnt);
			$("#update #notiCnt").val(data.notiCnt);
			$("#update input[type='time']").val(data.sdate.substr(11,5));
			
			if(data.rcvCnt==0) $("#update #rcvCnt").html("0").css("color","red");
			
			$("#updateForm").css("z-index",9999);	
			
			preRCV_QTY = $("#update #rcvCnt").html();
			preNOTI_QTY = $("#updateForm #notiCnt").val();
		}
	});
};

function getNewMatInfo(){
	var url = "${ctxPath}/chart/getNewMatInfo.do";
	var param = "prdNo=" + $("#group").val();
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType :"json",
		success : function(data){
			$("#insert #id").html(data.id);
			$("#insert #comName").html(data.comName);
			$("#insert #prdNo").html(data.prdNo);
			$("#insert #spec").html(data.spec);
			$("#insert #lotCnt").val(0);
			$("#insert #notiCnt").val(0);
			
			$("#insertForm").css("z-index",9999);	
		}
	});
};

$(function(){
	bindEvt();
	getPrdNo();
	$(".excel").click(csvSend);
	setEl();
	setDate();
	
	$("#menu_btn").click(function(){
		location.href = "${ctxPath}/chart/index.do"
	});
	
	//$(".date").change(getIncomStock);
	//$("#group").change(getIncomStock);
});
var className = "";
var classFlag = true;

function getStockStatus(){
	classFlag = true;
	var url = "${ctxPath}/chart/getStockStatus.do";
	
	var param = "prdNo=" + $("#group").val();
	
	console.log(param)
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.dataList;
			
			var tr = "<thead>" + 
						"<Tr style='background-color:#222222'>" +
							"<Td>" +
								"${prd_no}" +
							"</Td>" +
							"<Td>" +
								"${spec}" +
							"</Td>" +
							"<Td>" +
								"${division}" +
							"</Td>" +
							"<Td>" +
								"${income_cnt}" +
							"</Td>" +
							"<Td>" +  
								"${stock_cnt}" +
							"</Td>" +
						"</Tr></thead><tbody>";
						
			var ty = "";
			
			$(json).each(function(idx, data){
				if(data.prdNo.lastIndexOf("RW")!=-1){
					ty = "원소재";
				}else{
					ty = "완제품";
				};
				
				if(data.seq!="."){
					if(classFlag){
						className = "row2"
					}else{
						className = "row1"
					};
					classFlag = !classFlag;

					tr += "<tr class='contentTr " + className + "' id='tr" + data.id + "'>" +
								"<td >" + decode(data.prdNo) + "</td>" +
								"<td>" + data.spec + "</td>" +
								"<td>" + ty + "</td>" + 
								"<td>" + Number(data.rcvCnt) + "</td>" +
								"<td>" + Number(data.cnt) + "</td>" +
						"</tr>";					
				}
			});
			
			tr += "</tbody>";
			
			$(".alarmTable").html(tr).css({
				"font-size": getElSize(40),
			});
			
			$("button, input[type='time']").css("font-size", getElSize(40));
			
			$(".alarmTable td").css({
				"padding" : getElSize(20),
				"font-size": getElSize(40),
				"border": getElSize(5) + "px solid black"
			});
			
			$("#wrapper").css({
				"height" :getElSize(1650),
				"width" : "95%",
				"overflow" : "hidden"
			});
			
			$("#wrapper").css({
				"margin-left" : (contentWidth/2) -($("#wrapper").width()/2)
			});
			
			$(".row1").not(".tr_table_fix_header").css({
				"background-color" : "#222222"
			});

			$(".row2").not(".tr_table_fix_header").css({
				"background-color": "#323232"
			});
			
			$("#wrapper div:last").remove();
			scrolify($('#table'), getElSize(1450));
			$("#wrapper div:last").css("overflow", "auto")
		}
	});
};

function getBanner(){
	var url = "${ctxPath}/chart/getBanner.do";
	var param = "shopId=" + shopId;
	

	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			if(window.localStorage.getItem("banner")=="true"){
				$("#intro").html(data.msg).css({
					"color" : data.rgb,
					"right" : - window.innerWidth
				})
				
				$("#intro").html(data.msg).css({
					"right" : - $("#intro").width()
				})
			}
			
			bannerWidth = $("#intro").width() + getElSize(100);
			$("#intro").width(bannerWidth);
			$("#banner").val(data.msg);
			$("#color").val(data.rgb);
			
			twinkle();
			bannerAnim();
		}
	});		
}

var twinkle_opct = true;
function twinkle(){
	var opacity;
	if(twinkle_opct){
		opacity = 0;
	}else{
		opacity = 1;
	}
	$("#intro").css("opacity",opacity);
	
	twinkle_opct = !twinkle_opct;
	setTimeout(twinkle, 100)
};

var bannerWidth;
function bannerAnim(){
	$("#intro").width(bannerWidth - getElSize(10));
	$("#intro").animate({
		"right" : window.innerWidth  - getElSize(100)
	},15000, function(){
		$("#intro").css("right" , - $("#intro").width())
		//$(this).remove();
		bannerAnim();
	});
};
function chkBanner(){
	if(window.localStorage.getItem("banner")=="true"){
		getBanner();
		$("#intro_back").css("display","block");
	}else{
		$("#intro").html("");
		$("#intro_back").css("display","none");
	}
};

function cancelBanner(){
	window.localStorage.setItem("banner", "false");
	$("#bannerDiv").css("z-index",-9);
	chkBanner();
};

function addBanner(){
	var url = "${ctxPath}/chart/addBanner.do";
	var param = "msg=" + $("#banner").val() + 
				"&rgb=" + $("#color").val() + 
				"&shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success : function(data){
			window.localStorage.setItem("banner", "true");
			$("#bannerDiv").css("z-index",-9);
			location.reload();
			//chkBanner();
		}
	});
};

var panel = false;
function showPanel(){
	$("#panel").animate({
		"left" : 0
	});
	
	$("#menu_btn").animate({
		"left" : contentWidth * 0.2 + getElSize(50)	
	});
	
	$("#corver").css({
		"z-index" : 9,
		"opacity" : 0.7
	});
};

function closePanel(){
	$("#panel").animate({
		"left" : -contentWidth * 0.2 - getElSize(40)
	});
	
	$("#menu_btn").animate({
		"left" : 0
	});
	
	$("#corver").css({
		"z-index" : -1,
		"opacity" : 0.7
	});
};


function addZero(str){
	if(str.length==1){
		str = "0" + str;
	};
	return str;
};

function setDate(){
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth()+1));
	var day = addZero(String(date.getDate()));
	
	$(".date").val(year + "-" + month + "-" + day);
};


function clearMenu(){
	$("#corver").css({
		"z-index" : -1,
		"opacity" : 0
		
	});
	
};

function setEl() {
	$(".container").css({
		"width": contentWidth,
		"height" : contentHeight, 
		"margin-top" : originHeight/2 - ($(".container").height()/2)
	});
	
	$(".container").css({
		"margin-top" : originHeight/2 - ($(".container").height()/2),
		"margin-left" : originWidth/2 - ($(".container").width()/2),
	});
	
	$("#group").css({
		"font-size" : getElSize(50)	
	});
	
	$(".layoutTd").css({
		height : contentHeight * 0.85
	});

	$(".page").css({
		"height" : originHeight
	});

	$("#part1").css({
		"top" : 0
	});

	// 페이지 위치 조정
	for (var i = 2; i <= 5; i++) {
		$("#part" + i).css({
			"top" : $("#part" + (i - 1)).height() + originHeight
		});
	};

	$(".mainTable").css({
		"border-spacing" : getElSize(40)
	});

	$(".title").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(100)
	});

	$(".subTitle").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(50)
	});

	$(".tr2").css({
		"height" : contentHeight * 0.35
	})

	$("#chartTr").css({
		"height" : contentHeight * 0.4
	});

	$("#pie").css({
		"height" : contentHeight * 0.32
	});

	$(".neon1, .neon2, .neon3").css({
		"font-size" : getElSize(500)
	});

	$(".neon1, .neon2, .neon3").css({
		"margin-top" : $(".tr2").height() / 2 - $(".neon1").height()
						/ 2 - $(".subTitle").height(),
	});

	$("#machine_name").css({
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(170),
	});

	$("#machine_name_td").css({
		"height" : getElSize(500)
	});

	$("#machine_name").css({
		"margin-top" : $("#machine_name_td").height() / 2
						- $("#machine_name").height() / 2
						- $(".subTitle").height()
	});

	$(".title_left").css({
		"float" : "left",
		"width" : contentWidth * 0.1
	});

	$(".title_right").css({
		"float" : "right",
	});

	$("#menu_btn").css({
		"position" : "absolute",
		"width" : getElSize(130),
		"left" : 0,
		"top" : getElSize(20),
		"cursor" : "pointer",
		"z-index" : 13
	});
	

	$("#panel").css({
		"background-color" : "rgb(34,34,34)",
		"border" : getElSize(20) + "px solid rgb(50,50,50)",
		"position" : "absolute",
		"width" : contentWidth * 0.2,
		"top" : 0,
		"left" : -contentWidth * 0.2 - getElSize(40),
		"z-index" : 99999
	});

	$("#corver").css({
		"width" : originWidth,
		"height" : originHeight,
		"position" : "absolute",
		"z-index" : -1,
		"background-color" : "rgba(0,0,0,0.7)",
	});

	$("#panel_table").css({
		"color" : "white",
		"font-size" : getElSize(40)
	});

	$("#panel_table td").css({
		"padding" : getElSize(30),
		"cursor" : "pointer"
	});

	$(".dvcName_p1").css({
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});

	$(".wrap").css({
		"width" : contentWidth * 0.2,
		"padding" : getElSize(10),
		"border-radius" : getElSize(30),
		"padding-bottom" : getElSize(10),
		//"border": getElSize(10) + "px outset white",
	});
	
	$(".waitCnt, .alarmCnt, .noConCnt").css({
		"font-size" : getElSize(20),
		"margin" :getElSize(20)
	});
	
	$(".waitCnt").css({
		"color" : "yellow"
	});
	
	$(".alarmCnt").css({
		"color" : "red"
	});
	
	$(".noConCnt").css({
		"color" : "gray"
	});
	
	$(".card_status").css({
		"height" : getElSize(60)
	});
	
	$(".card_table").css({
		"border-spacing" : getElSize(10)
	});
	
	$(".opRatio").css({
		"font-size" :getElSize(100)
	});
	
	$(".comName").css({
		"font-size" :getElSize(60)
	});
	
	$(".machine_cam").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.08,
		//"border" : getElSize(10) + "px solid rgb(50,50,50)" 
	});
	
	$(".frame").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.15
	});
	
	$(".statusCnt").css({
		"height" : getElSize(70),
		"vertical-align" : "bottom",
		"box-shadow ": "inset 0 5px 0 #ddd"
	});
	
	$(".opTime").css({
		"font-weight" : "bolder",
		"top" : getElSize(400),
		"right" : getElSize(10)
	});
	
	$(".logo").css({
		"height" : contentWidth * 0.01
	});
	
	
	$("#title_left").css({
		"position" : "absolute",
		"width" : contentWidth * 0.07,
		"z-index" : 2,
		"left" : getElSize(100),
		"top" : getElSize(60)
	});

	$("#title_right").css({
		"position" : "absolute",
		"z-index" : 2,
		"right" : getElSize(100),
		"top" : getElSize(40),
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});
	
	$("#jigSelector, .goGraph, .excel, .label").css({
		"margin-right": getElSize(10),
		"margin-left": getElSize(10),
		"border-radius" : getElSize(10),
		"padding" : getElSize(10),
		"cursor": "pointer",
		"font-size": getElSize(50),
		"margin-bottom" : 0
	});
	
	$(".label").css({
		"margin-top" : 0
	});
	
	$(".date").css({
		"height": getElSize(70),
		"font-size": getElSize(50)
	});
	
	$(".thead").css({
		"font-size" : getElSize(60)
	});
	
	$(".alarmTable, .alarmTable tr, .alarmTable td").css({
		"border": getElSize(5) + "px solid rgb(50,50,50)"
	});
	
	$(".alarmTable td").css({
		"padding" : getElSize(10),
		"height": getElSize(100),
	});
	
	$(".contentTr").css({
		"font-size" : getElSize(60)
	});
	
	$(".machineListForTarget").css({
		"position" : "absolute",
		"width" : getElSize(1200),
		"height" : getElSize(1200),
		"overflow" : "auto",
		//"top" : getElSize(50),
		//"background-color" : "rgb(34,34,34)",
		"background-color" : "green",
		"color" : "white",
		"font-size" : getElSize(50),
		"padding" : getElSize(50),
		"overflow" : "auto",
		"border-radius" : getElSize(50),
		"border" : getElSize(10) + "px solid white",
	});
	
	$(".machineListForTarget").css({
		"left" : (originWidth/2) - ($(".machineListForTarget").width()/2),
	});
	
	$("#bannerDiv").css({
		"position" : "absolute",
		"width" : getElSize(1500),
		"height" : getElSize(200),
		"border-radius" : getElSize(20),
		"padding" : getElSize(50),
		"background-color" : "lightgray",
		"z-index" : -9
	});

	
	$("#bannerDiv").css({
		"left" : (originWidth/2) - ($("#bannerDiv").width()/2),
		"top" : (originHeight/2) - ($("#bannerDiv").height()/2)
	});
	
	$("#bannerDiv input[type='text']").css({
		"width" : getElSize(1200),
		"font-size" : getElSize(50)
	});
	
	$("#bannerDiv button").css({
		"margin" : getElSize(50),
		"font-size" : getElSize(50)
	});
	
	$("#intro").css({
		/* "width" : contentWidth, */
		"position" : "absolute",
		"bottom" : 0 + marginHeight,
		"font-size" : getElSize(140),
		"font-weight" : "bolder",
		"z-index" : 9999
	});
	
	$("#intro_back").css({
		"width" : originWidth,
		"display" : "none",
		"height" : getElSize(180),
		"opacity" : 0.3,
		"position" : "absolute",
		"background-color" : "white",
		"bottom" : 0 + marginHeight,
		"z-index" : 9999
	});
	
	$("#insertForm, #updateForm").css({
		"width" : getElSize(3500),
		"position" : "absolute",
		"z-index" : -999,
	});
	
	$("#insertForm, #updateForm").css({
		"left" : (originWidth/2) - ($("#insertForm").width()/2),
		"top" : (originHeight/2) - ($("#insertForm").height()/2)
	});
	
	$("#insert table td, #update table td").css({
		"color" : "white",
		"font-size" : getElSize(50),
		"padding" : getElSize(20),
		//"border" : getElSize(2) + "px solid black",
		"text-align" : "Center"
	});
	
	$("#delDiv").css({
		"position" : "absolute",
		"width" : getElSize(700),
		"height" :getElSize(200),
		"background-color" : "#444444",
		"color" : "white"
	});
	
	$("#delDiv").css({
		"top" : (window.innerHeight/2) - ($("#delDiv").height()/2),
		"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
		"z-index" : -1,
		"border-radius" : getElSize(10),
		"padding" : getElSize(20)
	});
	
	$("button").css({
		"font-size" : getElSize(40)	
	});
	
	chkBanner();
};


function addFaulty(el){
	var prdNo = $(el).parent("td").parent("tr").children("td:nth(2)").html();
	var cnt = $(el).parent("td").parent("tr").children("td:nth(7)").html();
	if(cnt.length>10){
		cnt = $(el).parent("td").parent("tr").children("td:nth(7)").children("input").val();
	}
	var url = "${ctxPath}/chart/addFaulty.do?addFaulty=true&prdNo=" + prdNo + "&cnt="+ cnt;
	
	location.href = url;
};

var csvOutput;
function csvSend(){
	var sDate, eDate;
	
	sDate = $("#sDate").val();
	eDate = $("#eDate").val();
	
	csvOutput = csvOutput.replace(/\n/gi,"");
	
	f.csv.value=encodeURIComponent(csvOutput);
	f.startDate.value = sDate;
	f.endDate.value = eDate;
	f.submit(); 
};

</script>
</head>

<body >
	<div id="bannerDiv">
		<Center>
			<input type="text" id="banner" size="100"><input type="color" id="color"><br>
			<button onclick="addBanner();">적용</button>
			<button onclick="cancelBanner();">미적용</button>
		</Center>
	</div>
	<div id="intro_back"></div>
	<span id="intro"></span>
	
	<div id="delDiv">
		<Center>
			<span>삭제하시겠습니까?</span><Br><br>
			<button id="resetOk" onclick="okDel();">삭제</button>&nbsp;&nbsp;
			<button id="resetNo" onclick="noDel();">취소</button>
		</Center>
	</div>
	
	<form action='${ctxPath}/csv/csv.do' id="f"  method="post">
		<input type="hidden" name="csv" value="">
		<input type="hidden" name="startDate" value="">
		<input type="hidden" name="endDate" value="">
	</form>
	<div id="title_right" class="title"></div>	
	
	<img src="${ctxPath }/images/home.png" id="menu_btn" >
	
					
	<div id="corver"></div>

	<div id="insertForm">
		<form action="" id="insert">
			<Table style="width: 100%">
				<tr style="background-color: #222222">
					<td>
						입고번호
					</td>
					<td>
						업체명
					</td>
					<td>
						품번
					</td>
					<td>
						규격
					</td>
					<td>
						Lot No
					</td>
					<td>
						로트수량
					</td>
					<td>
						검사수량
					</td>
					<td>
						불량수량
					</td>
					<td>
						입고수량
					</td>
					<td>
						불량내역
					</td>
					<td>
						입고일시
					</td>
				</tr>
				<Tr style="background-color: #323232">
					<Td id="id"></Td>
					<Td id="comName"></Td>
					<Td id="prdNo"></Td>
					<Td id="spec"></Td>
					<Td ><input type="text" id="lotNo" size="10"> </Td>
					<Td ><input type="text" id="lotCnt" size="10"> </Td>
					<Td id="smplCnt"></Td>
					<Td ><input type="text" id="notiCnt" size="10"> </Td>
					<Td id="rcvCnt"></Td>
					<td><button onclick='addFaulty(this)'>불량등록</button></td>
					<Td ><input type="time"> </Td>
				</Tr>
				<tr>
					<td colspan="11">
						<button id="save">확인</button>
						<button id="cancel">취소</button>
					</td>
				</tr>
			</Table>
		</form>
	</div>
	
	<div id="updateForm">
		<form action="" id="update">
			<Table style="width: 100%">
				<tr style="background-color: #222222">
					<td>
						입고번호
					</td>
					<td>
						업체명
					</td>
					<td>
						품번
					</td>
					<td>
						규격
					</td>
					<td>
						Lot No
					</td>
					<td>
						로트수량
					</td>
					<td>
						검사수량
					</td>
					<td>
						불량수량
					</td>
					<td>
						입고수량
					</td>
					<td>
						불량내역
					</td>
					<td>
						입고일시
					</td>
				</tr>
				<Tr style="background-color: #323232">
					<Td id="id"></Td>
					<Td id="comName"></Td>
					<Td id="prdNo"></Td>
					<Td id="spec"></Td>
					<Td ><input type="text" id="lotNo" size="10"> </Td>
					<Td ><input type="text" id="lotCnt" size="10"> </Td>
					<Td id="smplCnt"></Td>
					<Td ><input type="text" id="notiCnt" size="10"> </Td>
					<Td id="rcvCnt"></Td>
					<td><button onclick='addFaulty(this)'>불량등록</button></td>
					<Td ><input type="time"> </Td>
				</Tr>
				<tr>
					<td colspan="11">
						<button id="modify">확인</button>
						<button id="cancel">취소</button>
					</td>
				</tr>
			</Table>
		</form>
	</div>
	
	<!-- Part1 -->
	<div id="part1" class="page">
		<div class="container">
				<table class="mainTable">
					<tr>
						<Td align="center" style=" color:white; font-weight: bolder;" class="title" >
							<spring:message code="stock_status" ></spring:message>
						</Td>
					</tr>
				</table>
				
				<div style="width: 95%">
					<div class="label" style="float: right">
						<table>
							<Tr>
								<Td style="color: white"><spring:message code="prd_no" ></spring:message>&nbsp;<select id="group"> </select></Td>
							</Tr>
							<Tr>
								<Td align="right"><button onclick="getStockStatus()"><spring:message code="check" ></spring:message></button></Td>
							</Tr>
						</table>
					</div>
				</div>
				<div id="wrapper">
					<table style="width: 100%; color: white; text-align: center; border-collapse: collapse;" class="alarmTable" border="1" id="table">
						
					</table>
				</div>
		</div>
	</div>
</body>
</html>